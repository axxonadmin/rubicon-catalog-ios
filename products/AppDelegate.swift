//
//  AppDelegate.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func createAppWindow() -> UIWindow {
        return UIWindow(frame: UIScreen.main.bounds)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let window = createAppWindow()
        configureWindow(window)
        self.window = window
        
        customizeAppearance()
        
        return true
    }

    func configureWindow(_ window: UIWindow) {
        let main = MainViewController()
        window.rootViewController = main
        window.makeKeyAndVisible()
    }
    
    /// MARK: - App Theme Customization
        func customizeAppearance() {
            
            let toolbarColor = #colorLiteral(red: 0.137254902, green: 0.137254902, blue: 0.137254902, alpha: 1)
            
            UINavigationBar.appearance().backgroundColor = Theme.Colors.background
            UINavigationBar.appearance().tintColor = Theme.Colors.myColor //.systemGray //tint of the nav bar buttons
            UINavigationBar.appearance().barTintColor = Theme.Colors.background
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:Theme.Colors.label]
            UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor:Theme.Colors.label]
            
            let attrsNormal = [
                NSAttributedString.Key.foregroundColor: UIColor.lightGray,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)
            ]
            let attrsSelected = [
                NSAttributedString.Key.foregroundColor: Theme.Colors.myColor,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)
            ]
            UITabBarItem.appearance().setTitleTextAttributes(attrsNormal, for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes(attrsSelected, for: .selected)
    
            UITabBar.appearance().barTintColor = Theme.Colors.background
            UITabBar.appearance().tintColor = Theme.Colors.myColor
            UITabBar.appearance().isTranslucent = false
            
            UIToolbar.appearance().barTintColor = toolbarColor //Color.DGrayColor
    
            UIToolbar.appearance().tintColor = Theme.Colors.background
            UIToolbar.appearance().isTranslucent = false
            
            UISearchBar.appearance().barTintColor = .white
            UISearchBar.appearance().tintColor = Theme.Colors.myColor//.white
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.systemGray

            UISwitch.appearance().tintColor = Theme.Colors.myColor
            UISwitch.appearance().onTintColor = Theme.Colors.myColor
            //thumbTintColor
            UISlider.appearance().tintColor = Theme.Colors.myColor
            UISegmentedControl.appearance().tintColor = Theme.Colors.myColor
            
            self.window?.tintColor = Theme.Colors.myColor
            UIButton.appearance().tintColor = Theme.Colors.myColor
        }
}

