//
//  SubtitleTableViewCell.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = String(describing: SubtitleTableViewCell.self)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        accessoryType = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.textLabel?.text = nil
        self.detailTextLabel?.text = nil
        self.imageView?.image = nil
        self.accessoryType = .none
    }
}
