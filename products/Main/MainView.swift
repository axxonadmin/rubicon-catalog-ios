//
//  MainViewController.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//


import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let products = ProductsViewController()
        let info = InfoViewController()
        
        self.viewControllers = [
            UINavigationController(rootViewController:  products),
            UINavigationController(rootViewController:  info)
            //info
        ]
    }


}

