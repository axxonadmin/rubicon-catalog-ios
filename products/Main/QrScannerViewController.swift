//
//  QRScannerView.swift
//  Client
//
//  Created by Alexey Govorovsky on 13.04.2018.
//


import UIKit
import AVFoundation

class QrScannerViewController: UIViewController {
    
    var code: String?
    
    var messageLabel:UILabel! = {
        let view = UILabel()
        view.textColor = Theme.Colors.myColor
        return view
    }()
    
    var applyButton: UIButton! = {
        let view = UIButton()
        //view.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        view.layer.cornerRadius = 3
        view.contentHorizontalAlignment = .center
        //view.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        //view.titleColor = Theme.Colors.white
        view.setTitle("Поиск", for: .normal)
        view.backgroundColor = Theme.Colors.myColor
        view.isHidden = true
        return view
    }()
    
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.code128]
    
    let nothingFoundTitle = "Поиск ..."
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.title = nothingFoundTitle
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("QrScannerViewController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem =
                UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(close))
        } else {
            self.navigationItem.leftBarButtonItem =
                UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
        }
        
        
        // Get the back-facing camera for capturing videos
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera], mediaType: AVMediaType.video, position: .back)
        
//        if let cameraID = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.front).devices.first?.localizedName{
//            //cameraID = "Front Camera"
//        }
        
        //        let deviceDiscoverySession = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .back)

        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Move the message label and top bar to the front
        view.bringSubviewToFront(messageLabel)
        //view.bringSubview(toFront: topbar)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
        
        view.addSubview(applyButton)
        setupLayout()
        
        applyButton.addTarget(self, action: #selector(apply), for: .touchUpInside)
    }
    
    private func setupLayout() {
        applyButton.snp.makeConstraints {
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(48)
            $0.height.equalTo(48)
            $0.centerX.equalTo(view.snp.centerX)
            $0.width.equalTo(120)
            //$0.leading.equalToSuperview().offset(16)
            //$0.trailing.equalToSuperview().offset(-16)
        }
    }
    
    @objc func apply() {
        guard let code128 = self.code else { return }
        
        NotificationCenter.default.post(name: .highlightChanged, object: code128, userInfo: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension QrScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            self.title = nothingFoundTitle
            self.code = nil
            applyButton.isHidden = true
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if let stringValue = metadataObj.stringValue {
                //launchApp(decodedURL: metadataObj.stringValue!)
                //messageLabel.text = metadataObj.stringValue
                
                
                let split = stringValue.split(separator: "\u{1D}")
                if split.count == 2 && split[0].count == 4 {
                    let productCode = "\(split[0])"
                    let detectedTitle = Storage.shared.productTitleBy(code: productCode)
                    self.title = "\(detectedTitle ?? productCode) №\(split[1])"
                    self.code = detectedTitle
                    print("code \(title)")
                    applyButton.isHidden = false
                }
            }
        }
    }
    
}
