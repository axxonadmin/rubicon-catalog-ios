//
//  DocumentViewController.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import UIKit
import PDFKit

class DocumentViewController: UIViewController {

    var url: URL!
    
    lazy var pdfView: PDFView =  { [weak self] in
        let v = PDFView(frame: .zero)
        
        // Fit content in PDFView.
        v.autoScales = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        //self.title = "Document"
        self.tabBarItem.image = Theme.Images.catalog.resize(toWidth: 22)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        debugPrint("ProductsViewController deinit")
    }
    
    func setTransparentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.alpha = 0.2
        self.navigationController?.navigationBar.tintColor = Theme.Colors.myColor
        self.navigationController?.navigationBar.barTintColor = .clear//Theme1.Colors.transparentBlack
        self.navigationController?.navigationBar.backgroundColor = .clear//Theme1.Colors.transparentBlack
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func setNotTransparentNavigationBar() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    // MARK: Layout
    private func setupLayout() {
        pdfView.snp.makeConstraints {
            $0.edges.equalTo(view.snp.edges)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTransparentNavigationBar()
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem =
                UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(close))
        } else {
            self.navigationItem.leftBarButtonItem =
                UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(pdfViewVisiblePagesChanged(sender:)), name: .PDFViewVisiblePagesChanged, object: nil)
        
        // Add PDFView to view controller.
        //let pdfView = PDFView(frame: self.view.bounds)
        //pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(pdfView)
        
        setupLayout()
        
        
        // Load Sample.pdf file from app bundle.
        //let fileURL = Bundle.main.url(forResource: "Sample", withExtension: "pdf")
        //let fileURL = url//URL(string: "https://www.rubicon.ru/upload/iblock/bfd/bfd4396ebe668fcdcdcb29a4f97ae895.pdf")
        //pdfView.document = PDFDocument(url: fileURL!)
        pdfView.document = FileStorage.shared.load(url: url)
        
    }
    

    @objc func pdfViewVisiblePagesChanged(sender: Notification) {
        guard let doc = pdfView.document else { return }
        FileStorage.shared.save(url: url, pdfDocument: doc)
    }
    
    
    func share(_ sender: Any) {
        
        let data = pdfView.document!.dataRepresentation()
        var fileToshare = [Any]()
        
        fileToshare.append(data!)
        
        let shareAcivity = UIActivityViewController(activityItems: fileToshare, applicationActivities: nil)
        
        self.present(shareAcivity, animated: true, completion: nil)
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
}
