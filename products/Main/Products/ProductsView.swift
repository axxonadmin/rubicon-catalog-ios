//
//  ProductsViewController.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//


import UIKit
import SnapKit

extension Notification.Name {
    static let favoriteChanged = NSNotification.Name("ru.rubicon.products.favoriteChanged")
    static let highlightChanged = NSNotification.Name("ru.rubicon.products.highlightChanged")
}

class ProductsViewController: UIViewController {

    var favoriteOnly: Bool = false
    
    private func starImage() -> UIImage {
        return favoriteOnly
            ? Theme.Images.star1.resize(toWidth: 22)
            : Theme.Images.star0.resize(toWidth: 22)
    }
    
    let kCellIdentifier = "Cell"
    
    lazy var tableView: UITableView = { [weak self] in
        let v = UITableView()
        guard let `self` = self else { return v }
        
        v.dataSource = self
        v.delegate = self
        v.register(UITableViewCell.self, forCellReuseIdentifier: kCellIdentifier)
        v.register(SubtitleTableViewCell.self, forCellReuseIdentifier: SubtitleTableViewCell.reuseIdentifier)
        v.separatorStyle = .singleLine
        v.allowsSelection = true
        v.allowsMultipleSelection = false
        v.tableFooterView = UIView(frame: .zero)
        //v.tableFooterView = urlLabel
        
        v.backgroundColor = Theme.Colors.background
        return v
    }()
    
    lazy var searchController: UISearchController = { [weak self] in
        let empty = UISearchController(searchResultsController: nil)
        guard let `self` = self else { return empty }
        let c = UISearchController(searchResultsController: nil)
        c.searchResultsUpdater = self
        
        c.searchBar.sizeToFit()
        // For iOS 11 and later, we place the search bar in the navigation bar.
        //self.navigationItem.searchController = c
        // We want the search bar visible all the time.
        self.navigationItem.hidesSearchBarWhenScrolling = true
        
        c.delegate = self
        
        c.dimsBackgroundDuringPresentation = false // default is YES
        
        c.searchBar.delegate = self
        return c
    }()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.title = "Каталог"
        self.tabBarItem.image = Theme.Images.catalog.resize(toWidth: 22)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        debugPrint("\(#function)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = .yellow
        
        self.view.backgroundColor = Theme.Colors.background
        setupNavigationBarForLargeTitle()
        setupSearch()
        loadSubviews()
        setupLayout()
        
        
        
        let scabEnabled = true
        if scabEnabled {
            self.navigationItem.setRightBarButtonItems([
                UIBarButtonItem(image: self.starImage(), style: .plain, target: self, action: #selector(star)),
                UIBarButtonItem(image: Theme.Images.scanqr.resize(toWidth: 22), style: .plain, target: self, action: #selector(scanQr))
            ], animated: false)
        } else {
            self.navigationItem.rightBarButtonItem =
                UIBarButtonItem(image: self.starImage(), style: .plain, target: self, action: #selector(star))
        }
        NotificationCenter.default.addObserver(self, selector: #selector(favoriteChangedNotification), name: .favoriteChanged, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(highlightChangedNotification), name: .highlightChanged, object: nil)
    }
    
    @objc func favoriteChangedNotification() {
        debugPrint("\(#function)")
        tableView.reloadData()
    }
    
    private var highlightCellIndex: IndexPath?
    
    @objc func highlightChangedNotification(notification: Notification) {
        debugPrint("\(#function)")
        let x = notification.object as? String
        
        guard let title = x else { return }
        guard let productType = Storage.shared.productType(by: title) else { return } //[indexPath.section]
        let productTypes = Storage.shared.productTypes()
        guard let section = productTypes.firstIndex(of: productType) else { return }
        let items = Storage.shared.items(by: productType, favoriteOnly: self.favoriteOnly)
        guard let row = items.firstIndex(where: { $0.item.title == title }) else { return }
        let cellIndex = IndexPath(row: row, section: section)
        highlightCellIndex = cellIndex
        tableView.scrollToRow(at: cellIndex, at: .middle, animated: true)
        tableView.reloadRows(at: [cellIndex], with: .automatic)
//        let cell = self.tableView.cellForRow(at: cellIndex)
        //cell?.isHighlighted = true
//        UIView.animate(withDuration: 0.2, delay: 1.0, options: .curveLinear, animations: {
//            cell?.backgroundColor = Theme.Colors.myColor //UIColor.randomLightColor()
//        })
    }
    
    @objc func scanQr() {
        let vc = QrScannerViewController()
        let vcwrap = UINavigationController(rootViewController:  vc)
        self.navigationController?.present(vcwrap, animated: true, completion: nil)
    }
    
    @objc func applyFilter() {
    
    }
    
    @objc func star() {
        self.favoriteOnly = !self.favoriteOnly
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(image: self.starImage(), style: .plain, target: self, action: #selector(star))
        tableView.reloadData()
    }
    
    func setupNavigationBarForLargeTitle() {
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
        }
    }
    
    func setupSearch(){
            self.definesPresentationContext = true  //  viewWillDisappear to work by calling this! https://stackoverflow.com/a/33701499/2060780
            if #available(iOS 11.0, *) {
                self.navigationItem.searchController = self.searchController
    //            let resultsTableController = self
    //            searchController = UISearchController(searchResultsController: resultsTableController)
    //
    //            searchController.searchResultsUpdater = self
    //
    //            searchController.searchBar.sizeToFit()
    //            // For iOS 11 and later, we place the search bar in the navigation bar.
    //            self.navigationItem.searchController = searchController
    //            // We want the search bar visible all the time.
    //            self.navigationItem.hidesSearchBarWhenScrolling = false
    //
    //            searchController.delegate = self
    //
    //            searchController.dimsBackgroundDuringPresentation = false // default is YES
    //
    //            searchController.searchBar.delegate = self    // so we can monitor text changes + others
            }
        }
    
    private func loadSubviews() {
        view.addSubview(tableView)
    }
    
    // MARK: Layout
    private func setupLayout() {
        tableView.snp.makeConstraints {
            $0.edges.equalTo(view.safeAreaLayoutGuide.snp.edges).inset(UIEdgeInsets.zero)
        }
    }
}


// MARK: UITableViewDataSource
extension ProductsViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        debugPrint("\(#function)")
        return Storage.shared.productTypes().count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let productType = Storage.shared.productTypes()[section]
        let items = Storage.shared.items(by: productType, favoriteOnly: self.favoriteOnly)
        debugPrint("\(#function) items.count=\(items.count)")
        return items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let highlight = highlightCellIndex, indexPath == highlight {
            cell.backgroundColor = .systemGreen
        } else {
            cell.backgroundColor = .clear
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubtitleTableViewCell.reuseIdentifier, for: indexPath)
        
        //let sectionDay = eventsDays[indexPath.section]
        let productType = Storage.shared.productTypes()[indexPath.section]
        //let events1 = eventsByDay[sectionDay]!
        let items = Storage.shared.items(by: productType, favoriteOnly: self.favoriteOnly)
        //let event = events1[indexPath.row]
        let itemEx = items[indexPath.row]
        let item = itemEx.item
        
        cell.textLabel?.text = item.title
        cell.textLabel?.numberOfLines = 1
        cell.textLabel?.textColor = itemEx.favorite ? Theme.Colors.myColor : Theme.Colors.label
        cell.detailTextLabel?.text = item.name
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.textColor = itemEx.favorite ? Theme.Colors.myColor : Theme.Colors.label2
        //cell.imageView.image = ...
        //cell.accessoryType = .disclosureIndicator
//        ImageLoader.sharedLoader.imageForUrl(item.image, completionHandler: { (image, url) in
//            if(image != nil && cell.textLabel?.text == item.title){
//                cell.imageView?.image = image
//            }
//        })
        
        return cell
    }
    
}

//MARK: UITableViewDelegate
extension ProductsViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //let sectionDay = eventsDays[section]
        let productType = Storage.shared.productTypes()[section]
        //let eventsCount = eventsByDay[sectionDay]!.count
        let items = Storage.shared.items(by: productType, favoriteOnly: self.favoriteOnly)
        return "\(productType) (\(items.count))"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        guard let cell = tableView.cellForRow(at: indexPath) as? SubtitleTableViewCell else {
//            return
//        }
        
        let productType = Storage.shared.productTypes()[indexPath.section]
        let items = Storage.shared.items(by: productType, favoriteOnly: self.favoriteOnly)
        let itemEx = items[indexPath.row]
        
        showItem(item: itemEx)
        
    }
    
    private func showItem(item: ProductItemEx) {
        let vc = ProductViewController()
        vc.item = item
        
        let vcwrap = UINavigationController(rootViewController:  vc)
        //        vcwrap.hero.isEnabled = true
        //        vcwrap.hero.modalAnimationType = .selectBy(presenting: .fade, dismissing: .fade)
                
//        if #available(iOS 13.0, *) {
//            vcwrap.modalPresentationStyle = .fullScreen
//        }
                // lastly, present the view controller like normal
        self.navigationController?.present(vcwrap, animated: true, completion: nil)
        //self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - UISearchBarDelegate
extension ProductsViewController : UISearchBarDelegate {
    
//    private func filter(search: String) -> [Camera] {
//        if search.isEmpty {
//            return self.state.cameras
//        } else {
//            return self.state.cameras.filter({ (cam) -> Bool in
//                return cam.name.lowercased().contains(search.lowercased())
//            })
//        }
//    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        self.filtered = []
//        self.collectionView.reloadData()
//        self.filter = searchText
//        self.filtered = filter(search: searchText)
        Storage.shared.filter(search: searchText)
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        debugPrint("UISearchBarDelegate searchBarSearchButtonClicked.")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("UISearchBarDelegate searchBarCancelButtonClicked.")
//        self.filtered = []
//        self.tableView.reloadData()
//        self.filter = ""
//        self.filtered = filter(search: "")
//        self.tableView.reloadData()
        Storage.shared.filter(search: "")
        self.tableView.reloadData()
    }
}

// MARK: - UISearchControllerDelegate
extension ProductsViewController : UISearchControllerDelegate {
    func presentSearchController(_ searchController: UISearchController) {
        debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    func willPresentSearchController(_ searchController: UISearchController) {
        debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    func didPresentSearchController(_ searchController: UISearchController) {
        debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    func willDismissSearchController(_ searchController: UISearchController) {
        debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    func didDismissSearchController(_ searchController: UISearchController) {
        debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
}

// MARK: - UISearchResultsUpdating
extension ProductsViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if let text = searchController.searchBar.text, !text.isEmpty {
            debugPrint("updateSearchResults: filter: \(text).")
        }else{
            debugPrint("updateSearchResults: empty.")
        }
        // Update the filtered array based on the search text.
//        if let text = searchController.searchBar.text, !text.isEmpty {
//            self.filtered = self.countries.filter({ (country) -> Bool in
//                return country.lowercased().contains(text.lowercased())
//            })
//            self.filterring = true
//        }
//        else {
//            self.filterring = false
//            self.filtered = [String]()
//        }
//        self.table.reloadData()
    }
}
