//
//  ProductViewController.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import UIKit
import Eureka


class ProductViewController: FormViewController {

    var item: ProductItemEx!
    
    private func starImage() -> UIImage {
        return item.favorite
            ? Theme.Images.star1.resize(toWidth: 22)
            : Theme.Images.star0.resize(toWidth: 22)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        //self.title = "Product"
        //self.tabBarItem.image = Theme.Images.catalog.resize(toWidth: 22)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        debugPrint("\(#function)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = .yellow
        self.title = item.item.title
        
        if #available(iOS 13.0, *) {
            self.navigationItem.leftBarButtonItem =
                UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(close))
        } else {
            self.navigationItem.leftBarButtonItem =
                UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(close))
        }
        
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(image: starImage(), style: .plain, target: self, action: #selector(star))
        
        
        form
            //+++ logoSection(imageUrl: item.image)
            +++ section1()
    }
    
    private func showManual(_ manual: Manual) {
        
        let vc = DocumentViewController()
        vc.url = URL(string: manual.url)!
        //vc.title = item.title
        
        let vcwrap = UINavigationController(rootViewController:  vc)
        if #available(iOS 13.0, *) {
            vcwrap.modalPresentationStyle = .fullScreen
        }
        self.navigationController?.present(vcwrap, animated: true, completion: nil)
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func star() {
        item = ProductItemEx(item: item.item, favorite: !item.favorite)
        Storage.shared.set(name: item.item.name, favorite: item.favorite)
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(image: starImage(), style: .plain, target: self, action: #selector(star))
        
        NotificationCenter.default.post(name: .favoriteChanged, object: nil, userInfo: nil)
    }
    
    func section1() -> Eureka.Section {
        //LabelRow.defaultCellUpdate = { cell, row in cell.detailTextLabel?.textColor = UIColor.orange}
        let imageUrl: String = item.item.image
        let section = Eureka.Section() { section in
            section.header = {
                var header = HeaderFooterView<UIImageView>(.callback({
                    let view = UIImageView(frame: CGRect(x: 0, y: 0, width: 160, height: 160))
                    
                    ImageLoader.sharedLoader.imageForUrl(imageUrl, completionHandler: { (image, url) in
                        if(image != nil){
                            view.image = image
                        }
                    })
                    
                    view.contentMode = .scaleAspectFit
                    return view
                }))
                header.height = { 160 }
                return header
            }()
        }
        
        section <<< LabelRow(){
            $0.cellStyle = .subtitle
            $0.title = item.item.name
            $0.value = item.item.itemDescription            
            //$0.cell.detailTextLabel
            $0.cellUpdate({ cell, _ in
                cell.textLabel?.numberOfLines = 0
                cell.detailTextLabel?.numberOfLines = 0
            })
        }
            
        if let manual = item.item.manual {
            manual.forEach({ m in
                section <<< ButtonRow() {
                    $0.title = "\(m.name) [pdf, \(m.sizekb)KB]"//"Руководство по эксплуатации"//"Manual"
                }.onCellSelection { [weak self] (cell, row) in
                    self?.showManual(m)
                }
            })
        }
        
//        <<< ButtonRow() {
//            $0.title = "Руководство по эксплуатации"//"Manual"
//        }.onCellSelection { [weak self] (cell, row) in
//            self?.showManual()
//        }
//        <<< ButtonRow() {
//            $0.title = "Технические характеристики"//"Datasheet"
//        }.onCellSelection { [weak self] (cell, row) in
//            self?.showDatasheet()
//        }
        
        return section
    }
    
}
