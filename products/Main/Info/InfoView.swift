//
//  InfoViewController.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//


import UIKit
import Eureka

class InfoViewController: FormViewController {

    init() {
        super.init(nibName: nil, bundle: nil)
        self.title = "Ещё"
        self.tabBarItem.image = Theme.Images.me.resize(toWidth: 22)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        debugPrint("ProductsViewController deinit")
    }
    
    func setTransparentNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.alpha = 0.2
        self.navigationController?.navigationBar.tintColor = Theme.Colors.myColor
        self.navigationController?.navigationBar.barTintColor = .clear//Theme1.Colors.transparentBlack
        self.navigationController?.navigationBar.backgroundColor = .clear//Theme1.Colors.transparentBlack
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = UIView()
        setTransparentNavigationBar()
        //self.title = ""
        //self.view.backgroundColor = .yellow
        
        form
            +++ logoSection()
            +++ section1()
            +++ section2()
    }

    func fixHeightHeaderSection(height: CGFloat) -> Section {
        return Section { section in
            section.header = {
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                    view.backgroundColor = UIColor.orange
                    return view
                }))
                header.height = { height }
                return header
            }()
        }
    }
    
    func logoSection() -> Section {
        return Section { section in
            section.header = {
                var header = HeaderFooterView<UIImageView>(.callback({
                    let view = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                    view.image = Theme.Images.logo
                    view.contentMode = .scaleAspectFit
                    return view
                }))
                header.height = { 44 }
                return header
            }()
        }
    }
    
    func section1() -> Eureka.Section {
        
        return Eureka.Section()
//            <<< LabelRow() {
//                $0.cellStyle = .subtitle
//                $0.title = "Рубикон"
//                $0.value = "Российская компания, входящая в состав группы компаний ITV Group. Компания является официальным и единственным дистрибьютором адресной охранно-пожарной сигнализации «Рубикон». Оборудование «Рубикон» полностью сертифицировано, его разработка и производство осуществляется в России группой компаний «СИГМА»."
//                //$0.cell.detailTextLabel
//                $0.cellUpdate({ cell, _ in
//                    cell.textLabel?.numberOfLines = 0
//                    cell.detailTextLabel?.numberOfLines = 0
//                })
//
//            }
            
            <<< PhoneRow() {
                $0.title = "Телефон"
                $0.value = "8-800-77-001-77"
                $0.disabled = true
                $0.onCellSelection({ [weak self] (cell, row) in
                    debugPrint("phone")
                    let url = URL(string: "tel:8-800-77-001-77")
                    UIApplication.shared.open(url!, options: [:])
                })
            }
//            <<< ActionSheetRow<String>() {
//                $0.title = "Телефон"
//                $0.selectorTitle = "Позвонить?"
//                $0.options = ["8-800-77-001-77"]
//                $0.value = "8-800-77-001-77"
//                $0.onPresent { from, to in
//                    to.popoverPresentationController?.permittedArrowDirections = .up
//                }
//                $0.onCellSelection({ cell, row in
//                    print("phone")
//                })
//            }
            
            <<< EmailRow() {
                $0.title = "Email"
                $0.value = "info@rubicon.ru"
                $0.disabled = true
                $0.onCellSelection({ [weak self] (cell, row) in
                    debugPrint("email")
                    let url = URL(string: "mailto:info@rubicon.ru")
                    UIApplication.shared.open(url!, options: [:])
                })
            }
            <<< URLRow() {
                $0.title = "Сайт"
                $0.value = URL(string: "https://www.rubicon.ru/")
                $0.disabled = true
                $0.onCellSelection({ [weak self] (cell, row) in
                    debugPrint("url")
                    let url = URL(string: "https://www.rubicon.ru/")
                    UIApplication.shared.open(url!, options: [:])
                })
            }
            
    }
    
    func section2() -> Eureka.Section {
        return Eureka.Section()
//        <<< ButtonRow() {
//            $0.title = "Обновить каталог"
//        }.onCellSelection { [weak self] (cell, row) in
//            self?.update()
//        }
//        <<< ButtonRow() {
//            $0.title = "Загрузить всё"
//        }.onCellSelection { [weak self] (cell, row) in
//            self?.downloadAll()
//        }
        <<< ButtonRow() {
            $0.title = "Очистить кэш"   //TODO: отобразить сколько всего уже в кэше
        }.onCellSelection { [weak self] (cell, row) in
            self?.clearAll()
        }
    }
    
    private func downloadAll() {
        debugPrint("downloadAll")
    }
    
    private func clearAll() {
        debugPrint("clearAll")
        FileStorage.shared.clear()
    }
}

