//
//  ProductsResult.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import Foundation

// MARK: - ProductCode
struct ProductCode: Codable {
    let code: String?
    let title: String
}

// MARK: - ProductItem
struct ProductItem: Codable {
    let path: String
    let title, name, itemDescription: String
    let image: String
    let productType: String
    let manual: [Manual]?

    enum CodingKeys: String, CodingKey {
        case path, title, name
        case itemDescription = "description"
        case image
        case productType = "product_type"
        case manual
    }
}

// MARK: - Manual
struct Manual: Codable {
    let name: String
    let url: String
    let sizekb: Int

    enum CodingKeys: String, CodingKey {
        case name, url
        case sizekb = "size_kb"
    }
}

class ProductItemEx {
    let item: ProductItem
    let favorite: Bool
    
    init(item: ProductItem, favorite: Bool) {
        self.item = item
        self.favorite = favorite
    }
}

typealias ProductsResult = [ProductItem]


let testProductJsonString = """
[
  {
    "product_type": "Приемно-контрольные приборы",
    "index": 0,
    "items": [
      {
        "index": 0,
        "title": "ППК-Е",
        "name": "ППК-Е (Прибор приемно-контрольный, без дисплея)",
        "description": "ППК-E не имеет собственной панели управления и дисплея: настройка, управление и индикация состояний выполняются с помощью внешних устройств. ",
        "image": "https://www.rubicon.ru/upload/iblock/624/624c95d3d6068709e71e3c36599d737b.jpg",
        "manual": "https://www.rubicon.ru/upload/iblock/bfd/bfd4396ebe668fcdcdcb29a4f97ae895.pdf",
        "datasheet": "https://www.rubicon.ru/upload/iblock/894/8949cacb0cc7f865a52be52cf36156b0.pdf"
      },
      {
        "index": 1,
        "title": "ППК-М",
        "name": "Рубикон ППК-М (Прибор приемно-контрольный, графический дисплей)",
        "description": "ППК-М – единственный из ППК Рубикон, имеющий собственную панель управления и дисплей. Также доступна настройка с ПК, подключенного по интерфейсу RS-485. ППК-М предназначен для применения в системах безопасности и автоматического управления оборудованием на малых и средних объектах. ",
        "image": "https://www.rubicon.ru/upload/iblock/330/330b44686ea210dc6582da2916e164f3.png",
        "manual": "https://www.rubicon.ru/upload/iblock/bfd/bfd4396ebe668fcdcdcb29a4f97ae895.pdf",
        "datasheet": "https://www.rubicon.ru/upload/iblock/894/8949cacb0cc7f865a52be52cf36156b0.pdf"
      }
    ]
  },
  {
    "product_type": "Извещатели",
    "index": 1,
    "items": [ ]
  }
]
"""
