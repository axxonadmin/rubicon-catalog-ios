//
//  FileStorage.swift
//  products
//
//  Created by Alexey Govorovsky on 12.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import Foundation
import PDFKit

class FileStorage {

    public class var shared : FileStorage {
        struct Static {
            static let instance : FileStorage = FileStorage()
        }
        return Static.instance
    }

    private init() {
    }
    
    public func clear() {
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let docURL = documentDirectory.appendingPathComponent("pdf")
        if fileManager.fileExists(atPath: docURL.path){
            try? fileManager.removeItem(atPath: docURL.path)
        }
    }
    
    public func save(url: URL, pdfDocument: PDFDocument){
        if let docUrl = pdfDocument.documentURL, docUrl.isFileURL {
            return
        }
        // Get the raw data of your PDF document
        let data = pdfDocument.dataRepresentation()
        let documentDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let docRelativePathFromUrl = url.path //starts with /url.replacingOccurrences(of: "https://rubicon.ru/", with: "")
        let docURL = documentDirectory.appendingPathComponent("pdf\(docRelativePathFromUrl)")
        let dir = docURL.deletingLastPathComponent()
        do {
            try? FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true)
            try data?.write(to: docURL)
        } catch(let error) {
            debugPrint("error is \(error.localizedDescription)")
        }
    }
    
    public func load(url: URL) -> PDFDocument? {
        let fileManager = FileManager.default
        let documentDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let docRelativePathFromUrl = url.path //starts with /
        let docURL = documentDirectory.appendingPathComponent("pdf\(docRelativePathFromUrl)")
        
        if fileManager.fileExists(atPath: docURL.path){
            return PDFDocument(url: docURL)
        }
        else{
            print("file does not exist..")
            return PDFDocument(url: url)
        }
    }
}


