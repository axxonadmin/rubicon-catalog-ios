//
//  Storage.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//

import Foundation

class Storage {
    private let result: ProductsResult
    private let productCodes: [ProductCode]
    private let types: [String]
    private var search: String = ""
    private var favorites: [String] = [] //names of fav items
    
    public class var shared : Storage {
        struct Static {
            static let instance : Storage = Storage()
        }
        return Static.instance
    }
    
    private init() {
        let str = json_string(fromFile: "rubicon")!
        result = try! JSONDecoder().decode(ProductsResult.self, from: str.data(using: .utf8)!)
        let strCodes = json_string(fromFile: "rubicon_codes")!
        productCodes = try! JSONDecoder().decode([ProductCode].self, from: strCodes.data(using: .utf8)!)
        types = Array<String>(Set<String>(result.map({$0.productType})))
        favorites = UserDefaults.standard.stringArray(forKey: "favorites") ?? []
    }
    
    public func productTitleBy(code: String) -> String? {
        return productCodes
            .filter({ $0.code != nil })
            .filter({ $0.code! == code })
            .first?
            .title
    }
    
    public func productType(by title: String) -> String? {
        return result
            .filter({ $0.title == title })
            .map({ $0.productType })
            .first
    }
    
    public func productTypes() -> [String] {
        return types.sorted()
    }
    
    public func items(by type: String, favoriteOnly: Bool) -> [ProductItemEx] {
        return result
            .filter({$0.productType == type})
            .map({ item in ProductItemEx(item: item, favorite: favorites.contains(item.name))})
            .filter({ favoriteOnly ? $0.favorite : true })
            .filter({ (x) -> Bool in
                return search.isEmpty
                    || x.item.title.lowercased().contains(search.lowercased())
                    || x.item.name.lowercased().contains(search.lowercased())
                    || x.item.itemDescription.lowercased().contains(search.lowercased())
            })
            .sorted(by: { (a,b) in a.item.title < b.item.title })
    }
    
    public func filter(search: String) {
        self.search = search.lowercased()
    }
    
    public func set(name: String, favorite: Bool) {
        if favorite {
            if !favorites.contains(name) {
                favorites.append(name)
                UserDefaults.standard.set(favorites, forKey: "favorites")
                UserDefaults.standard.synchronize()
            }
        } else {
            if favorites.contains(name) {
                favorites.removeAll(where: { $0 == name })
                UserDefaults.standard.set(favorites, forKey: "favorites")
                UserDefaults.standard.synchronize()
            }
        }
        
    }
}

func json(fromFile file: String) -> Any? {
  return Bundle(for: JSONFileReader.self).path(forResource: file, ofType: "json")
    .flatMap { URL(fileURLWithPath: $0) }
    .flatMap { try? Data(contentsOf: $0) }
    .flatMap(JSONObjectWithData)
}

func json_string(fromFile file: String) -> String? {
    return Bundle(for: JSONFileReader.self).path(forResource: file, ofType: "json")
        .flatMap { URL(fileURLWithPath: $0) }
        .flatMap { try? Data(contentsOf: $0) }
        .flatMap(StringWithData)
}

func xml_string(fromFile file: String) -> String? {
    return Bundle(for: JSONFileReader.self).path(forResource: file, ofType: "xml")
        .flatMap { URL(fileURLWithPath: $0) }
        .flatMap { try? Data(contentsOf: $0) }
        .flatMap(StringWithData)
}

private func JSONObjectWithData(fromData data: Data) -> Any? {
  return try? JSONSerialization.jsonObject(with: data, options: [])
}

private func StringWithData(fromData data: Data) -> String {
    return String(data: data, encoding: .utf8)!
}

private class JSONFileReader { }
