//
//  Theme.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//


import UIKit

final class Theme {
    final class Images {
        static let scanqr = UIImage(named: "scan-qr")!
        static let catalog = UIImage(named: "catalog")!
        static let me = UIImage(named: "icon-1")!
        static let logo = UIImage(named: "logo")!
        
        static let star0 = UIImage(named: "star-empty")!
        static let star1 = UIImage(named: "star-filled")!
    }
    
    final class Colors {
            
            static let myColor:UIColor = {
                return UIColor.systemRed
            }()
            
            static let white = UIColor.white
            static let black = UIColor.black
            static let clear = UIColor.clear
            
            static let background: UIColor = {
                if #available(iOS 13.0, *) {
                    return .systemBackground
                } else {
                    return .white
                }
            }()
            
            static let label: UIColor = {
                if #available(iOS 13.0, *) {
                    return .label
                } else {
                    return .black
                }
            }()
            
            static let label2: UIColor = {
                if #available(iOS 13.0, *) {
                    return .secondaryLabel
                } else {
                    return .darkGray
                }
            }()
            
            static let label3: UIColor = {
                if #available(iOS 13.0, *) {
                    return .tertiaryLabel
                } else {
                    return .lightGray
                }
            }()
            
            
            static let transparentBlack = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
            static let transparentGray = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 0.7)
            static let transparentGray2 = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 0.3)
            static let transparentYellow = UIColor(red: 0.7, green: 0.7, blue: 0, alpha: 0.3)
            
        }
    
    final class Controls {
        static let internalMargin = 8
        static let roundButtonSize = 32
        
        static func createOverlayRoundButton(_ image: UIImage) -> UIButton {
            let b = UIButton()
            b.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            b.layer.cornerRadius = CGFloat(roundButtonSize / 2)
            b.contentHorizontalAlignment = .center
            b.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            //colors
            b.setImage(image, for: UIControl.State.normal)
            return b
        }
        
        static func createOverlayRoundTextButton(_ text: String) -> UIButton {
            let b = UIButton()
            //b.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            b.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            b.layer.cornerRadius = CGFloat(roundButtonSize / 2)
            b.contentHorizontalAlignment = .center
            b.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            //colors
            b.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.footnote)
            b.titleLabel?.numberOfLines = 1
            b.titleLabel?.textAlignment = .center
            b.titleLabel?.adjustsFontSizeToFitWidth = true
            b.titleLabel?.minimumScaleFactor = 0.5
            b.setTitle(text, for: .normal)
            b.setTitleColor(Theme.Colors.label, for: .normal)
            
            let cornerRadius: CGFloat = 22
    
            b.backgroundColor = Theme.Colors.background
            b.layer.cornerRadius = cornerRadius
            b.layer.masksToBounds = false
            b.layer.shadowColor = Theme.Colors.label.cgColor
            b.layer.shadowOffset = .zero
            b.layer.shadowOpacity = Float(0.5)
            b.layer.shadowRadius = CGFloat(2)
    
            return b
        }
    }
}
