//
//  UIImageExtensions.swift
//  products
//
//  Created by Alexey Govorovsky on 10.12.2019.
//  Copyright © 2019 ITV/AxxonSoft. All rights reserved.
//


import UIKit


extension UIImage {

    /**
     *  reset image size
     */
    func resize(toSize:CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(toSize, false, UIScreen.main.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: toSize.width, height: toSize.height))
        if let reSizeImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return reSizeImage
        }
        return UIImage()
    }
    
    func resize(toWidth:CGFloat) -> UIImage {
        return resize(toSize: CGSize(width: toWidth, height: toWidth))
    }
}

extension UIImage{
    func resize2(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}
